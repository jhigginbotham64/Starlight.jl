# Starlight.jl

A greedy game engine for [greedy programmers](https://julialang.org/blog/2012/02/why-we-created-julia/)!

Borrows heavily from the [MIT-licensed GameZero](https://github.com/aviks/GameZero.jl/blob/master/LICENSE).

COMING SOON!

## Roadmap
- Artifact/Asset loading (images, sound, etc.)
- Sprites
- Text
- Audio
- Game compilation workflow
- Round of examples and testing, to include input events (v0.2.0)
- 3D drawing
- 3D physics
- 2D physics (easier to subtract the 3rd dimension than to add it)
- Round of examples and testing, to include Pong (v0.3.0)
- AI/Behaviors library
- Networking/Multiplayer
- Round of examples and testing, to include Bomberman (v0.4.0)
- Distributed Systems
- Round of examples and testing, to include Constellation (v0.5.0)
- CI/CD
- Documentation (v1.0.0)
- ???
- Profit
